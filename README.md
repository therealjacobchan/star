# Star

NOTES FROM THE DEVELOPER:

# Installation

Using the Terminal, Go to the project folder

```sh
$ cd $HOME/Desktop
```


### Presentation

Checkout release branch SSH
```sh
$ git clone git@bitbucket.org:therealjacobchan/star.git
```

Checkout release branch HTTP
```sh
$ git clone https://therealjacobchan@bitbucket.org/therealjacobchan/star.git
```

Pull the most updated changes
```sh
$ git pull  
```


### Cocoapods setup

Install all the libraries in cocoapods. Requires Cocoapods 1.8.4
```sh
$ pod install
```

### Xcode Setup

In Xcode, Build > Clean and then Compile!

NOTE: This will require Xcode 11 and iOS 13.
