//
//  StarTests.swift
//  StarTests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest

@testable import Star

final class StarTests: XCTestCase {
    let dummyJSON: [String: Any] = [
     "resultCount": 1,
     "results": [
        [
            "wrapperType": "track",
            "kind": "feature-movie",
            "collectionId": 1476858599,
            "trackId": 1437031362,
            "artistName": "Bradley Cooper",
            "collectionName": "A Star is Born 3 Film Collection",
            "trackName": "A Star Is Born (2018)",
            "collectionCensoredName": "A Star is Born 3 Film Collection",
            "trackCensoredName": "A Star Is Born (2018)",
            "collectionArtistId": 199257486,
            "collectionArtistViewUrl": "https://itunes.apple.com/au/artist/warner-bros-entertainment-inc/199257486?uo=4",
            "collectionViewUrl": "https://itunes.apple.com/au/movie/a-star-is-born-2018/id1437031362?uo=4",
            "trackViewUrl": "https://itunes.apple.com/au/movie/a-star-is-born-2018/id1437031362?uo=4",
            "previewUrl": "https://video-ssl.itunes.apple.com/itunes-assets/Video128/v4/6b/cd/60/6bcd60b0-73ce-1a9e-1bf8-d7bcc8d32c10/mzvf_2708740245690387686.640x356.h264lc.U.p.m4v",
            "artworkUrl30": "https://is2-ssl.mzstatic.com/image/thumb/Video128/v4/75/6e/46/756e4680-9493-f58c-3d3f-947d36e5c32a/source/30x30bb.jpg",
            "artworkUrl60": "https://is2-ssl.mzstatic.com/image/thumb/Video128/v4/75/6e/46/756e4680-9493-f58c-3d3f-947d36e5c32a/source/60x60bb.jpg",
            "artworkUrl100": "https://is2-ssl.mzstatic.com/image/thumb/Video128/v4/75/6e/46/756e4680-9493-f58c-3d3f-947d36e5c32a/source/100x100bb.jpg",
            "collectionPrice": 14.99,
            "trackPrice": 14.99,
            "trackRentalPrice": 4.99000,
            "collectionHdPrice": 14.99000,
            "trackHdPrice": 14.99000,
            "trackHdRentalPrice": 4.99000,
            "releaseDate": "2018-10-18T07:00:00Z",
            "collectionExplicitness": "notExplicit",
            "trackExplicitness": "notExplicit",
            "discCount": 1,
            "discNumber": 1,
            "trackCount": 3,
            "trackNumber": 3,
            "trackTimeMillis": 8148723,
            "country": "AUS",
            "currency": "AUD",
            "primaryGenreName": "Drama",
            "contentAdvisoryRating": "M",
            "shortDescription": "Seasoned musician Jackson Maine (Bradley Cooper) discovers—and falls in love with—struggling artist",
            "longDescription": "Seasoned musician Jackson Maine (Bradley Cooper) discovers—and falls in love with—struggling artist Ally (Lady Gaga). She has just about given up on her dream to make it big as a singer… until Jack coaxes her into the spotlight. But even as Ally’s career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
            "hasITunesExtras": true ]
        ]
    ]
    func testDecode() throws {
        let json = try JSONSerialization.data(withJSONObject: dummyJSON)
        let starResponse = try JSONDecoder().decode(StarResponse<Star>.self, from: json)
        let star = starResponse.results
        XCTAssertEqual(starResponse.resultCount, star.count)
    }
}
