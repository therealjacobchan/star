//
//  ViewDetailsViewModelTests.swift
//  StarTests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest

@testable import Star

final class ViewDetailsViewModelTests: XCTestCase {
    func testInit() {
        let star = Star()
        let viewModel = ViewDetailsViewModel(star: star)
        XCTAssertNil(viewModel.star.artistName)
    }
}
