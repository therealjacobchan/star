//
//  StarTests.swift
//  StarTests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest
@testable import Star

final class ViewModelTests: XCTestCase {
    func testFetchSuccess() {
        let expection = expectation(description: "Expecting success on fetching")
        let mockService = MockedStarService(result: true)
        let viewModel = ViewModel(starService: mockService)
        viewModel.successfulFetch = { (result, response) in
            XCTAssertTrue(result)
            XCTAssertEqual(response, "")
            expection.fulfill()
        }
        viewModel.fetchStar()
        waitForExpectations(timeout: 5)
    }

    func testFetchFail() {
        let expection = expectation(description: "Expecting fail on fetching")
        let mockService = MockedStarService(result: false)
        let viewModel = ViewModel(starService: mockService)
        viewModel.successfulFetch = { (result, response) in
            XCTAssertFalse(result)
            XCTAssertNotEqual(response, "")
            expection.fulfill()
        }
        viewModel.fetchStar()
        waitForExpectations(timeout: 5)
    }
}
