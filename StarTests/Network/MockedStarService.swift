//
//  MockedStarService.swift
//  StarTests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest

import AlamofireImage
import Star

class MockedStarService: StarServiceProtocol {

    let result: Bool

    init(result: Bool) {
        self.result = result
    }

    func getResults(completion: @escaping (APIResponse<[Star]?>) -> Void) -> CancelableRequest {
        if result {
            completion(.success([Star()]))
        } else {
            completion(.failure(NSError(domain: "Failed", code: 0, userInfo: nil)))
        }
        return MockCancelableRequest()
    }
}

struct MockCancelableRequest: CancelableRequest {
    func cancel() -> MockCancelableRequest {
        return MockCancelableRequest()
    }
}
