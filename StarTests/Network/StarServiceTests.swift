//
//  StarServiceTests.swift
//  StarTests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest
import AlamofireImage

@testable import Star

final class StarServiceTests: XCTestCase {

    func testGetStar() {
        let expect = expectation(description: "Successful fetching using service")

        var star: [Star]?
        let service = StarService()
        service.getResults { [weak self] (response) in
            guard self != nil else { return }
            switch response {
            case .success(let str):
                star = str
            default:
                break
            }
            expect.fulfill()
        }

        waitForExpectations(timeout: 600) { (error) in
            XCTAssertNotNil(star)
            if let star = star {
                if CommandLine.arguments.contains("-mockResponse") {
                    XCTAssertEqual(star.count, 1)
                } else {
                    XCTAssertEqual(star.count, 50)
                }
            }
        }
    }
}
