//
//  APIResponse.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

/// Generic API response that is either success or failure
///
/// - success: generic type
/// - failure: error type
public enum APIResponse<T> {
    case success(T)
    case failure(Error)
}
