//
//  StarService.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Alamofire

public class StarService: StarServiceProtocol {

    @discardableResult
    public func getResults(completion: @escaping (APIResponse<[Star]?>) -> Void) -> CancelableRequest {
        let url = "https://itunes.apple.com/search?term=star&country=au&media=movie"

        return AF.request(url).validate().responseJSON { (response) in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    if let data = response.data, let star = try? JSONDecoder().decode(StarResponse<Star>.self, from: data) {
                        completion(.success(star.results))
                    } else {
                        let error = NSError(domain: "Connection error", code: -1, userInfo: nil)
                        completion(.failure(error))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
