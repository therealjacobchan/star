//
//  CancelableRequest.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Alamofire

public protocol CancelableRequest {
    @discardableResult
    func cancel() -> Self
}

extension DataRequest: CancelableRequest { }
