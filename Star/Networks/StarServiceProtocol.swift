//
//  StarServiceProtocol.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

public protocol StarServiceProtocol {
    @discardableResult
    func getResults(completion: @escaping (APIResponse<[Star]?>) -> Void) -> CancelableRequest
}
