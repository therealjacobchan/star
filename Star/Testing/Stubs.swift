//
//  Stubs.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import OHHTTPStubs
import Foundation

#if DEBUG
extension AppDelegate {
    func stubResponses() {
        stubStar()
        stubImage()

        UserDefaults.standard.set(false, forKey: "1437031362")

        UIView.setAnimationsEnabled(false)
    }

    func stubStar() {
        stub(condition: { (request) -> Bool in
            return request.url?.absoluteString == "https://itunes.apple.com/search?term=star&country=au&media=movie" && request.httpMethod == "GET"
        }) { (_) -> HTTPStubsResponse in
            let stubPath = OHPathForFile("Star.json", type(of: self))
            return fixture(filePath: stubPath ?? "", headers: ["Content-Type": "application/json"])
        }
    }

    func stubImage() {
        stub(condition: { (request) -> Bool in
            return (request.url?.absoluteString.contains(".jpg") ?? false) && request.httpMethod == "GET"
        }) { (_) -> HTTPStubsResponse in
            let stubPath = OHPathForFile("Star.jpg", type(of: self))
            return fixture(filePath: stubPath ?? "", headers: ["Content-Type": "image"])
        }
    }
}
#endif
