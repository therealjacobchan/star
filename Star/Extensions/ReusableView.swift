//
//  ReusableView.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public protocol ReusableView {
    static var defaultReuseIdentifier: String {
        get
    }
}

extension ReusableView where Self: UIView{
    public static var defaultReuseIdentifier: String{
        return String(describing: self)
    }
}
