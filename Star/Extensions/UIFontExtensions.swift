//
//  UIFontExtensions.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

extension UIFont {
    static let sizeLimitByStyle = [ UIFont.TextStyle.title1: 38, // default 28
        UIFont.TextStyle.title2: 32, // default 22
        UIFont.TextStyle.title3: 30, // default 20
        UIFont.TextStyle.headline: 27, // default 17
        UIFont.TextStyle.subheadline: 25, // default 15
        UIFont.TextStyle.body: 27, // default 17
        UIFont.TextStyle.callout: 26, // default 16
        UIFont.TextStyle.footnote: 23, // default 13
        UIFont.TextStyle.caption1: 24, // default 12
        UIFont.TextStyle.caption2: 22, // default 11
        UIFont.TextStyle.largeTitle: 44//default 34
    ]

    static func limitedPreferredFontForTextStyle(style: UIFont.TextStyle) -> UIFont {
        var sizeLimit: CGFloat = CGFloat(Float.greatestFiniteMagnitude)
        if let limit = sizeLimitByStyle[style] {
            sizeLimit = CGFloat(limit)
        }
        return preferredFont(withTextStyle: style, maxSize: sizeLimit)
    }

    static func preferredFont(withTextStyle textStyle: UIFont.TextStyle, maxSize: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: textStyle)

        let font = UIFont(descriptor: fontDescriptor, size: fontDescriptor.pointSize)
        let maxFont = UIFont(descriptor: fontDescriptor, size: maxSize)
        return fontDescriptor.pointSize <= maxSize ? font : maxFont
    }
}
