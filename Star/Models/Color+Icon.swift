//
//  Color.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public struct Color {
    public static let starTint = UIColor(named: "StarTint")
    public static let favorite = UIColor(named: "Favorite")
    public static let unfavorite = UIColor(named: "Unfavorite")
}

public struct Icon {
    public static let star = UIImage(named: "Star")
}
