//
//  UserDefaultsCredentials.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

public final class UserDefaultCredentials {
    let userDefaults: UserDefaults
    public init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }

    public func getBoolValue(key: String) -> Bool {
        return self.userDefaults.bool(forKey: key)
    }
    public func set(value: Bool, key: String){
        self.userDefaults.set(value, forKey: key)
    }
}
