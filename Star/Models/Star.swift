//
//  Star.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

public class Star: Decodable {
    public var wrapperType: String?
    public var kind: String?
    public var collectionId: Int?
    public var trackId: Int?
    public var artistName: String?
    public var collectionName: String?
    public var trackName: String?
    public var collectionCensoredName: String?
    public var trackCensoredName: String?
    public var collectionArtistId: Int?
    public var collectionArtistViewUrl: String?
    public var collectionViewUrl: String?
    public var trackViewUrl: String?
    public var previewUrl: String?
    public var artworkUrl30: String?
    public var artworkUrl60: String?
    public var artworkUrl100: String?
    public var collectionPrice: Float?
    public var trackPrice: Float?
    public var trackRentalPrice: Double?
    public var collectionHdPrice: Double?
    public var trackHdPrice: Double?
    public var trackHdRentalPrice: Double?
    public var releaseDate: String?
    public var collectionExplicitness: String?
    public var trackExplicitness: String?
    public var discCount: Int?
    public var discNumber: Int?
    public var trackCount: Int?
    public var trackNumber: Int?
    public var trackTimeMillis: Int?
    public var country: String?
    public var currency: String?
    public var primaryGenreName: String?
    public var contentAdvisoryRating: String? //M PG G MA15+
    public var shortDescription: String?
    public var longDescription: String?
    public var hasITunesExtras: Bool?

    public init() {

    }
}
