//
//  ViewController.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public class ViewController: UIViewController {

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isAccessibilityElement = true
        tableView.accessibilityIdentifier = "tableView"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.registerCell(TableViewCell.self)
        tableView.separatorInset = .zero
        tableView.backgroundColor = .systemBackground
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = true
        return tableView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private let viewModel: ViewModel

    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Star"
        self.navigationController?.navigationBar.accessibilityIdentifier = "mainNavigationBar"

        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = UIRectEdge.top
        view.addSubview(tableView)
        view.addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor)
        ])
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)

        self.activityIndicatorView.startAnimating()
        viewModel.successfulFetch = { [weak self] (result, response) in
            self?.activityIndicatorView.stopAnimating()
            if result {
                self?.tableView.reloadData()
            }
        }
        viewModel.fetchStar()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedPath = tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedPath, animated: true)
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCount
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.isUserInteractionEnabled = true
        cell.isAccessibilityElement = true
        cell.accessibilityIdentifier = "tableCell_\(indexPath.row)"
        let star = viewModel.results[indexPath.row]
        cell.configure(star: star)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let star = viewModel.results[indexPath.row]
        let viewModel = ViewDetailsViewModel(star: star)
        viewModel.favoritePressed = { [weak tableView] in
            if let selectedPath = tableView?.indexPathForSelectedRow {
                tableView?.beginUpdates()
                tableView?.reloadRows(at: [selectedPath], with: .automatic)
                tableView?.endUpdates()
            }
        }
        viewModel.dismissPressed = { [weak tableView] in
            if let selectedPath = tableView?.indexPathForSelectedRow {
                tableView?.beginUpdates()
                tableView?.reloadRows(at: [selectedPath], with: .automatic)
                tableView?.endUpdates()
            }
        }
        let viewController = ViewDetailsViewController(viewModel: viewModel)
        let navigationController = NavigationController(rootViewController: viewController)
        navigationController.presentationController?.delegate = self
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
}

extension ViewController: UIAdaptivePresentationControllerDelegate {
    public func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }
}
