//
//  TableViewCell.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit
import AlamofireImage

class TableViewCell: UITableViewCell, ReusableView {

    private lazy var profileImageView: UIImageView = {
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        profileImageView.isAccessibilityElement = true
        profileImageView.clipsToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.isAccessibilityElement = true
        profileImageView.accessibilityIdentifier = "profileImageView"
        return profileImageView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.adjustsFontForContentSizeCategory = true
        nameLabel.isAccessibilityElement = true
        nameLabel.accessibilityIdentifier = "nameLabel"
        nameLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .headline)
        nameLabel.numberOfLines = 0
        return nameLabel
    }()

    private lazy var genreLabel: UILabel = {
        let genreLabel = UILabel()
        genreLabel.translatesAutoresizingMaskIntoConstraints = false
        genreLabel.adjustsFontForContentSizeCategory = true
        genreLabel.isAccessibilityElement = true
        genreLabel.accessibilityIdentifier = "genreLabel"
        genreLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        genreLabel.numberOfLines = 0
        genreLabel.textColor = UIColor.lightGray
        return genreLabel
    }()

    private lazy var favoriteImageView: UIImageView = {
        let favoriteImageView = UIImageView()
        favoriteImageView.image = Icon.star?.withRenderingMode(.alwaysTemplate)
        favoriteImageView.isAccessibilityElement = true
        favoriteImageView.accessibilityIdentifier = "favoriteImageView"
        favoriteImageView.tintColor = Color.starTint
        favoriteImageView.translatesAutoresizingMaskIntoConstraints = false
        favoriteImageView.clipsToBounds = true
        favoriteImageView.contentMode = .scaleAspectFill
        return favoriteImageView
    }()

    private lazy var middleStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 5
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.addArrangedSubview(self.genreLabel)
        stackView.addArrangedSubview(self.favoriteImageView)
        NSLayoutConstraint.activate([
            self.favoriteImageView.heightAnchor.constraint(equalToConstant: 25),
            self.favoriteImageView.widthAnchor.constraint(equalToConstant: 25)
        ])
        return stackView
    }()

    private lazy var priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.adjustsFontForContentSizeCategory = true
        priceLabel.isAccessibilityElement = true
        priceLabel.accessibilityIdentifier = "priceLabel"
        priceLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        priceLabel.numberOfLines = 0
        priceLabel.textColor = UIColor.lightGray
        return priceLabel
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 2
        stackView.addArrangedSubview(self.nameLabel)
        stackView.addArrangedSubview(self.genreLabel)
        stackView.addArrangedSubview(self.priceLabel)
        return stackView
    }()

    private lazy var userDefaults: UserDefaultCredentials = {
        let userDefaults = UserDefaultCredentials()
        return userDefaults
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureUI() {
        if profileImageView.superview == nil {
            contentView.addSubview(activityIndicatorView)
            contentView.addSubview(profileImageView)
            contentView.addSubview(nameLabel)
            contentView.addSubview(middleStackView)
            contentView.addSubview(priceLabel)
            NSLayoutConstraint.activate([
                profileImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                profileImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10),
                profileImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
                profileImageView.heightAnchor.constraint(equalToConstant: 100),
                profileImageView.widthAnchor.constraint(equalToConstant: 100),
                profileImageView.trailingAnchor.constraint(equalTo: middleStackView.leadingAnchor, constant: -10),
                activityIndicatorView.centerXAnchor.constraint(equalTo: profileImageView.centerXAnchor),
                activityIndicatorView.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor),

                nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                nameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 10),
                nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
                nameLabel.bottomAnchor.constraint(equalTo: middleStackView.topAnchor, constant: -10),

                middleStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                middleStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),

                priceLabel.topAnchor.constraint(equalTo: middleStackView.bottomAnchor, constant: 10),
                priceLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 10),
                priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
                priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
            ])
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.af.cancelImageRequest()
        profileImageView.image = nil
    }

    func configure(star: Star) {
        self.nameLabel.text = star.trackName ?? ""
        self.genreLabel.text = star.primaryGenreName ?? ""
        self.priceLabel.text = "A$\(star.trackPrice ?? 0.0)"
        self.activityIndicatorView.startAnimating()
        self.favoriteImageView.isHidden = !userDefaults.getBoolValue(key: "\(star.trackId ?? 0)")
        if let url = URL(string: star.artworkUrl100 ?? "") {
            self.profileImageView.af.setImage(withURL: url) { (response) in
                self.activityIndicatorView.stopAnimating()
            }
        }
    }
}
