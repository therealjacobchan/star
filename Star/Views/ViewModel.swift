//
//  ViewModel.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public class ViewModel: NSObject {
    var results: [Star]
    var totalCount = 0
    let starService: StarServiceProtocol

    public var successfulFetch: ((Bool, String) -> Void)? = nil

    public convenience override init() {
        self.init(starService: StarService())
    }
    public init(starService: StarServiceProtocol) {
        self.results = [Star]()
        self.starService = starService
    }

    public func fetchStar() {
        self.starService.getResults { [weak self] (response) in
            switch response {
            case .success(let star):
                if let star = star {
                    self?.results = star
                    self?.totalCount = star.count
                }
                self?.successfulFetch?(true, "")
            case .failure(let error):
                self?.successfulFetch?(false, error.localizedDescription)
            }
        }
    }
}
