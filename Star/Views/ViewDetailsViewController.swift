//
//  ViewDetailsViewController.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public class ViewDetailsViewController: UIViewController {

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        imageView.isAccessibilityElement = true
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.isAccessibilityElement = true
        imageView.accessibilityIdentifier = "imageView"
        imageView.backgroundColor = UIColor.black
        return imageView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let avatarActivityIndicator = UIActivityIndicatorView()
        avatarActivityIndicator.color = UIColor.black
        avatarActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        avatarActivityIndicator.hidesWhenStopped = true
        return avatarActivityIndicator
    }()

    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.adjustsFontForContentSizeCategory = true
        descriptionLabel.isAccessibilityElement = true
        descriptionLabel.accessibilityIdentifier = "descriptionLabel"
        descriptionLabel.font = UIFont.limitedPreferredFontForTextStyle(style: .callout)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = UIColor.lightGray
        descriptionLabel.textAlignment = .center
        return descriptionLabel
    }()

    private lazy var favoriteButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isAccessibilityElement = true
        button.accessibilityIdentifier = "favoriteButton"
        button.layer.cornerRadius = 10.0
        button.backgroundColor = Color.favorite
        button.setTitle("Add to Favorites", for: .normal)
        button.titleLabel?.font = UIFont.limitedPreferredFontForTextStyle(style: .headline)
        button.tintColor = UIColor.white
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(self.favoritesPressed(_:)), for: .touchUpInside)
        return button
    }()

    private let viewModel: ViewDetailsViewModel
    private var doneButton: UIBarButtonItem!

    public init(viewModel: ViewDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        self.title = self.viewModel.star.trackName ?? ""
        self.navigationController?.navigationBar.accessibilityIdentifier = "detailsNavigationBar"
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.activityIndicatorView)
        self.view.addSubview(self.descriptionLabel)
        self.view.addSubview(self.favoriteButton)
        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 75),
            self.imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            self.imageView.heightAnchor.constraint(equalTo: self.imageView.widthAnchor, multiplier: 1.0),
            self.imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.descriptionLabel.topAnchor, constant: 25),

            self.activityIndicatorView.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor),
            self.activityIndicatorView.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor),

            self.descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            self.descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            self.descriptionLabel.bottomAnchor.constraint(equalTo: self.favoriteButton.topAnchor, constant: -10),

            self.favoriteButton.leadingAnchor.constraint(equalTo: self.descriptionLabel.leadingAnchor),
            self.favoriteButton.trailingAnchor.constraint(equalTo: self.descriptionLabel.trailingAnchor),
            self.favoriteButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            self.favoriteButton.heightAnchor.constraint(equalToConstant: 40)
        ])

        self.descriptionLabel.text = self.viewModel.star.longDescription ?? ""
        let isFavorite = viewModel.userDefaults.getBoolValue(key: "\(viewModel.star.trackId ?? 0)")
        self.favoriteButton.setTitle(!isFavorite ? "Add to Favorites" : "Remove from Favorites", for: .normal)
        self.favoriteButton.backgroundColor = isFavorite ? Color.unfavorite : Color.favorite

        self.activityIndicatorView.startAnimating()
        if let url = URL(string: viewModel.star.artworkUrl100 ?? "") {
            self.imageView.af.setImage(withURL: url) { [weak self] (response) in
                self?.activityIndicatorView.stopAnimating()
            }
        }
        doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.dismissVC))
        self.navigationItem.rightBarButtonItem = doneButton

    }

    @objc
    func favoritesPressed(_ sender: UIButton) {
        self.favoriteButton.isEnabled = false
        self.doneButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.favoriteButton.isEnabled = true
            self.doneButton.isEnabled = true
            var isFavorite = self.viewModel.userDefaults.getBoolValue(key: "\(self.viewModel.star.trackId ?? 0)")
            isFavorite.toggle()
            self.viewModel.userDefaults.set(value: isFavorite, key: "\(self.viewModel.star.trackId ?? 0)")
            self.favoriteButton.setTitle(!isFavorite ? "Add to Favorites" : "Remove from Favorites", for: .normal)
            self.favoriteButton.backgroundColor = isFavorite ? Color.unfavorite : Color.favorite
            self.viewModel.favoritePressed?()
        }
    }

    @objc
    func dismissVC() {
        self.viewModel.dismissPressed?()
        self.dismiss(animated: true, completion: nil)
    }
}
