//
//  RootCoordinator.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import UIKit

public class RootCoordinator {
    private var window: UIWindow?
    private let navigationController: NavigationController = NavigationController()

    func start(window: UIWindow?) {
        self.window = window

        window?.rootViewController = configureRoot()

        window?.makeKeyAndVisible()
    }

    private func configureRoot() -> UIViewController {
        let viewModel = ViewModel()
        let viewController = ViewController(viewModel: viewModel)
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.setViewControllers([viewController], animated: true)

        return navigationController
    }
}

public class NavigationController: UINavigationController {
    public override var prefersStatusBarHidden: Bool {
        return true
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
//        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.navigationTitle]
//        navigationBar.tintColor = Color.navigationTitle
//        navigationBar.barTintColor = Color.navigationBackground
        navigationBar.isTranslucent = true
    }
}
