//
//  ViewDetailsViewModel.swift
//  Star
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import Foundation

public class ViewDetailsViewModel: NSObject {
    public let star: Star
    public let userDefaults: UserDefaultCredentials
    public var favoritePressed: (() -> ())? = nil
    public var dismissPressed: (() -> ())? = nil
    init(star: Star){
        self.star = star
        self.userDefaults = UserDefaultCredentials()
    }
}
