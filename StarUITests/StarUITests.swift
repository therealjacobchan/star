//
//  StarUITests.swift
//  StarUITests
//
//  Created by Jacob Chan on 5/29/20.
//  Copyright © 2020 Clear. All rights reserved.
//

import XCTest

final class StarUITests: XCTestCase {
    var app = XCUIApplication()
    let shouldStubRequests = true

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        if shouldStubRequests {
            app.launchArguments = ["-mockResponse"]
        }
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
        app.terminate()
    }

    func testMainScreen() {
        givenIStartApp()
        thenIShouldBeInMainScreen()
    }

    func testCellTap() {
        givenIStartApp()
        andITapCell()
        thenIShouldBeInDetails()
    }

    func testFavorites() {
        givenIStartApp()
        andITapCell()
        thenIShouldBeInDetails()
        andIPressFavorites()
        andIReturnToMainScreen()
        thenIShouldSeeChangesInItem1()
    }

    func thenIShouldSeeChangesInItem1() {
        XCTContext.runActivity(named: "Then I should see the item changed") { _ in
            let cell = app.tables["tableView"].cells.element(matching: .cell, identifier: "tableCell_0")
            wait(forElement: cell.images["favoriteImageView"])
        }
    }

    func andIPressFavorites() {
        XCTContext.runActivity(named: "And favorites has been tapped") { _ in
            app.buttons["favoriteButton"].tap()
        }
    }

    func andIReturnToMainScreen() {
        XCTContext.runActivity(named: "And user returns to main screen") { _ in
            wait(forElement: app.navigationBars["detailsNavigationBar"].buttons["Done"], timeout: 2)
            app.navigationBars["detailsNavigationBar"].buttons["Done"].tap()
        }
    }

    func thenIShouldBeInDetails() {
        XCTContext.runActivity(named: "Then I should be in the details screen") { _ in
            wait(forElement: app.navigationBars["detailsNavigationBar"])
            wait(forElement: app.images["imageView"])
            wait(forElement: app.buttons["favoriteButton"])
            wait(forElement: app.staticTexts["descriptionLabel"])
        }
    }

    func andITapCell() {
        XCTContext.runActivity(named: "And I tap a cell") { _ in
            let cell = app.tables["tableView"].cells.element(matching: .cell, identifier: "tableCell_0")
            cell.staticTexts["nameLabel"].tap()
        }
    }

    func givenIStartApp() {
        XCTContext.runActivity(named: "Given the application is ready") { _ in
            XCTAssertEqual(app.state, .runningForeground)
        }
    }

    func thenIShouldBeInMainScreen() {
        XCTContext.runActivity(named: "Then I should be in the main screen") { _ in
            wait(forElement: app.navigationBars["mainNavigationBar"])
            let table = app.tables["tableView"]
            wait(forElement: table)

            let cell = app.tables["tableView"].cells.element(matching: .cell, identifier: "tableCell_0")
            wait(forElement: cell)
            wait(forElement: cell.images["profileImageView"])
            wait(forElement: cell.staticTexts["nameLabel"])
            wait(forElement: cell.staticTexts["genreLabel"])
            wait(forElement: cell.staticTexts["priceLabel"])
        }
    }
}

extension XCTestCase {
    func wait(forElement element: XCUIElement, timeout: TimeInterval = 5) {
        let predicate = NSPredicate(format: "exists == 1")
        let expection = expectation(for: predicate, evaluatedWith: element, handler: nil)
        let waitResult = XCTWaiter.wait(for: [expection], timeout: timeout)
        switch waitResult {
        case .completed:
            break
        default:
            XCTFail("Failed to find element \(element)")
        }
    }
}
